import { Message, Channel } from "../types";

export default function createBackendMock() {
  let data: Record<string, Channel> = {};
  let messages: Record<string, Message[]> = {};
  let listeners: Record<string, Array<(ev: MessageEvent) => void>> = {};

  window.addEventListener("message", (msg) => {
    if (msg.data.type === "talky:debug:message") {
      Object.keys(listeners).forEach((channelId) => {
        postMessage(channelId, msg.data.data);
      });
    }
  });

  function createChannel(name: string, channelId: string) {
    data = { ...data, [channelId]: { name, messages: [] } };
    messages = { ...messages, [channelId]: [] };
    listeners = { ...listeners, [channelId]: [] };
  }

  function getChannel(channelId: string) {
    return data[channelId];
  }

  function postMessage(channelId: string, message: Message) {
    messages = {
      ...messages,
      [channelId]: messages[channelId]
        ? [...messages[channelId], message]
        : [message]
    };

    listeners[channelId].forEach((l) =>
      l({
        data: JSON.stringify({ type: "PUSH_MESSAGE", payload: message })
      } as MessageEvent)
    );
  }

  function getStream(channelId: string) {
    return {
      addEventListener(type: "message", listener: (ev: MessageEvent) => void) {
        listeners[channelId] = listeners[channelId]
          ? [listener, ...listeners[channelId]]
          : [listener];

        listener({
          data: JSON.stringify({
            type: "LOAD_MESSAGES",
            payload: [{ from: "pat", time: new Date(), msg: "hello there" }]
          })
        } as MessageEvent);
      },
      close() {}
    };
  }

  return {
    createChannel,
    getChannel,
    postMessage,
    getStream
  };
}

export type BackendMock = ReturnType<typeof createBackendMock>;
