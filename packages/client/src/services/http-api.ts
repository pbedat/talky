import { ApiClient } from "../types";

export default function createApiClient(baseUrl: string): ApiClient {
  return {
    async callChannel(channelId: string) {
      return [];
    },
    async postMessage(args: { channelId: string; text: string; from: string }) {
      const { channelId, text, from } = args;
      const response = await fetch(`${baseUrl}/${channelId}/messages`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          text,
          from
        })
      });

      return response.ok ? [] : [{ error: response.text() }];
    }
  };
}
