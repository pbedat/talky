import { Channel, ApiClient } from "../types";
import { BackendMock } from "./backend-mock";

export default function createInMemoryApi(mockBackend: BackendMock): ApiClient {
  return {
    async postMessage(args) {
      mockBackend.postMessage(args.channelId, {
        isResponse: false,
        text: args.text,
        time: new Date().toISOString(),
        from: args.from
      });

      return [];
    },
    async callChannel(channelId) {
      return [];
    }
  };
}
