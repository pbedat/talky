export interface Channel {
  name?: string;
  messages: Message[];
}

export interface Message {
  isResponse?: boolean;
  from: string;
  time: string;
  text: string;
}

export interface ApiClient {
  callChannel(channelId: string): Promise<Either>;
  postMessage(args: {
    channelId: string;
    text: string;
    from: string;
  }): Promise<Either>;
}

export type Either<TResult = void> = TResult extends void
  ? [] | [unknown]
  : [unknown] | [undefined, TResult];
