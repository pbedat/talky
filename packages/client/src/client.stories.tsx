import React from "react";
import { storiesOf } from "@storybook/react";

storiesOf("client", module).add("default", () => (
  <div className="bg-white focus:outline-none focus:shadow-outline border border-gray-300 rounded-lg py-2 px-4 block w-full appearance-none leading-normal">
    hello
  </div>
));
