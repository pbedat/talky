import React from "react";

interface Props {
  isOnline?: boolean;
}

const OnlineStatus: React.FC<Props> = (props) => {
  const { isOnline } = props;

  return (
    <em>
      <span style={{ color: isOnline ? "green" : "gray" }}>●</span>{" "}
      {isOnline
        ? "I'm online and ready to talk"
        : "I'm offline, but I'll answer you later"}
    </em>
  );
};

export default OnlineStatus;
