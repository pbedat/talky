import { storiesOf } from "@storybook/react";
import React from "react";

import CreateChannel from "./CreateChannel";
import { boolean } from "@storybook/addon-knobs";

storiesOf("CreateChannel", module).add("default", () => (
  <CreateChannel isOnline={boolean("isOnline", false)} />
));
