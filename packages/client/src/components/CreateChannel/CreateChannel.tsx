import React, { useState, useCallback } from "react";
import OnlineStatus from "../OnlineStatus";
import Notifications from "../Notifications/notifications";
const p = require("../../../assets/profile.jpg");

interface Props {
  isOnline?: boolean;
  name?: string;
  onCreate?(name: string): void;
}

const CreateChannel: React.FC<Props> = props => {
  const { isOnline, onCreate } = props;

  const [name, setName] = useState(props.name!);
  const createChannel = useCallback(() => onCreate!(name.trim()), [name]);

  return (
    <form onSubmit={createChannel}>
      <div
        style={{
          height: 200,
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center 20%",
          backgroundSize: "100%",
          backgroundImage: `url(${p})`
        }}
      ></div>
      <h1>Hey there, let's chat!</h1>

      <div style={{ marginBottom: "1rem" }}>
        <Notifications />
      </div>

      <OnlineStatus isOnline={isOnline} />
      <p>
        <input
          placeholder="Your name (optional)"
          value={name}
          onChange={ev => setName(ev.currentTarget.value)}
        />
      </p>
      <button onClick={createChannel}>
        {name.trim().length ? `Talk to me, ${name}` : "Stay anonymous"}
      </button>
    </form>
  );
};

export default CreateChannel;

CreateChannel.defaultProps = {
  onCreate: () => {},
  name: ""
};
