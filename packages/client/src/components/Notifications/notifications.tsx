import React, { useState, useCallback, useMemo } from "react";

const Notifications: React.FC = () => {
  const wasEnabled = useMemo(() => Notification.permission === "granted", []);

  const [requestingPermission, setRequestingPermission] = useState(false);
  const [enabled, setEnabled] = useState(Notification.permission === "granted");

  const enable = useCallback(async () => {
    if (enabled) {
      return;
    }
    setRequestingPermission(true);
    const permission = await Notification.requestPermission();

    setRequestingPermission(false);

    if (permission === "granted") {
      setEnabled(true);
    }
  }, []);

  if (wasEnabled) {
    return null;
  }

  if (Notification.permission === "denied") {
    return (
      <em style={{ color: "gray" }}>
        Notifications for this site are blocked. You can change this in the page
        settings of your browser.
      </em>
    );
  }

  return enabled ? (
    <span>Thank you!</span>
  ) : (
    <div>
      Notifications:
      <label>
        <input
          type="radio"
          checked={enabled}
          onClick={enable}
          disabled={requestingPermission}
        />{" "}
        Yes{requestingPermission ? "..." : ""}
      </label>
      <label>
        <input
          type="radio"
          checked={!enabled}
          disabled={requestingPermission}
        />{" "}
        No
      </label>
    </div>
  );
};

export default Notifications;
