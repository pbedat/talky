import styled from "styled-components";

const Root = styled.div`
  font-family: "Segoe UI", Tahoma, Geneva, Verdana, sans-serif;
  position: fixed;
  right: 2rem;
  bottom: 2rem;
`;

const Body = styled.div`
  background: white;
  border: 1px silver;
  max-width: calc(100vw - 4rem);
  padding: 2rem;

  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
`;

const Close = styled.span`
  position: absolute;
  top: 0.5rem;
  right: 0.5rem;
  padding: 0.25rem;
  cursor: pointer;
`;

export default { Body, Close, Root };
