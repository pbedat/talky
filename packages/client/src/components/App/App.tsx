import React, { useState, useCallback, useRef } from "react";

import Communicator from "../Communicator";
import CreateChannel from "../CreateChannel";
import TalkButton from "../TalkButton";
import useTalky from "../../hooks/use-talky";
import styled from "./App.styles";
import ShadowRoot from "../ShadowRoot";

const App: React.FC = props => {
  const [name, setName] = useState(localStorage.getItem("talky:name"));
  const { submitMessage, messages, isOnline } = useTalky();
  const [isOpen, setIsOpen] = useState(false);

  const createName = useCallback(name => {
    setName(name);
    localStorage.setItem("talky:name", name);
  }, []);

  return (
    <ShadowRoot>
      <styled.Root>
        {isOpen ? (
          <styled.Body>
            <styled.Close onClick={() => setIsOpen(!isOpen)}>
              &times;
            </styled.Close>
            {name !== null ? (
              <Communicator
                isOnline={isOnline}
                name={name}
                onMessage={text => {
                  submitMessage(name, text);
                }}
                messages={messages}
              />
            ) : (
              <CreateChannel isOnline={isOnline} onCreate={createName} />
            )}
          </styled.Body>
        ) : (
          <TalkButton onClick={() => setIsOpen(!isOpen)} />
        )}
      </styled.Root>
    </ShadowRoot>
  );
};

export default App;
