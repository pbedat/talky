import { storiesOf } from "@storybook/react";
import React from "react";

import App from "./App";
import DependencyContainer from "../../hooks/di";
import configure from "../../di";

storiesOf("App", module).add("default", () => (
  <DependencyContainer configure={() => configure("http://localhost:8080")}>
    <App />
  </DependencyContainer>
));
