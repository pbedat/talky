import styled from "styled-components";

const Message = styled.div<{ isResponse?: boolean }>`
  margin-bottom: 1rem;
  text-align: ${({ isResponse }) => (isResponse ? "left" : "right")};
`;

const Root = styled.div``;

const MessageInput = styled.form`
  display: flex;

  textarea {
    flex-grow: 1;
  }

  button {
  }
`;

const Messages = styled.div`
  margin-top: 1rem;
  max-height: 30vh;
  overflow-y: scroll;

  small {
    color: gray;
  }

  p {
    margin-top: 0;
    white-space: pre-line;
  }
`;

export default { Root, Message, Messages, MessageInput };
