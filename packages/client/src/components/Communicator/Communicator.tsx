import React, { useRef, useEffect, useState, useCallback } from "react";
import { format } from "date-fns";
import autosize from "autosize";

import styled from "./Communicator.styles";
import { Channel, Message } from "../../types";
import OnlineStatus from "../OnlineStatus";

interface Props {
  name: string;
  messages?: Message[];
  isOnline?: boolean;

  onMessage?(msg: string): void;
  onClose?(): void;
}

const Communicator: React.FC<Props> = props => {
  const { isOnline, name, messages, onMessage, onClose } = props;

  const inputRef = useRef<HTMLTextAreaElement | null>(null);

  useEffect(() => {
    if (inputRef.current !== null) {
      autosize(inputRef.current);
    }
    return () => {
      autosize.destroy(inputRef.current!);
    };
  }, []);

  const messagesRef = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    messagesRef.current?.lastElementChild?.scrollIntoView();
  }, [messages]);

  const [message, setMessage] = useState("");
  const submit = useCallback(
    (ev: any) => {
      ev.preventDefault();

      if (message.replace(/\s/, "").length) {
        if (message) {
          onMessage!(message);
        }
        setMessage("");
      }

      setTimeout(() => autosize.update(inputRef.current!));
    },
    [message]
  );

  const close = useCallback(() => onClose!(), []);

  return (
    <styled.Root>
      <h1>Hey{name.length ? ` ${name}` : ""}, nice to meet you!</h1>
      <OnlineStatus isOnline={isOnline} />
      <styled.Messages ref={messagesRef}>
        {messages!.map(({ time, from, text: msg, isResponse }) => (
          <styled.Message key={time} isResponse={isResponse}>
            <small>{format(new Date(time), "yyyy-MM-dd HH:mm")}</small>
            <p>{msg}</p>
          </styled.Message>
        ))}
      </styled.Messages>
      <styled.MessageInput onSubmit={submit}>
        <textarea
          ref={inputRef}
          rows={1}
          value={message}
          onChange={ev => setMessage(ev.target.value)}
          onKeyDown={ev => {
            ev.key === "Enter" && !ev.shiftKey && submit(ev);
          }}
        ></textarea>
        <button type="submit">Send</button>
      </styled.MessageInput>
    </styled.Root>
  );
};

export default Communicator;

Communicator.defaultProps = {
  messages: [],
  onMessage: () => {},
  onClose: () => {}
};
