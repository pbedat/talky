import { storiesOf } from "@storybook/react";
import { withKnobs, boolean } from "@storybook/addon-knobs";

import React from "react";

import Communicator from "./Communicator";

storiesOf("Communicator", module)
  .addDecorator(withKnobs)
  .add("default", () => (
    <Communicator
      isOnline={boolean("Online", false)}
      name="Dude"
      messages={[
        { from: "Dude", time: "2019-01-01T13:00:00", text: "hello" },
        {
          from: "Patrick",
          time: "2019-01-01T13:30:00",
          text: "sup?",
          isResponse: true
        }
      ]}
    />
  ));
