import { useRef, useState } from "react";
import React from "react";
import root from "react-shadow";
import { StyleSheetManager } from "styled-components";

const DidMount = (props: { onMount(): void }) => {
  React.useEffect(props.onMount, []);

  return null;
};

const ShadowRoot: React.FC = (props) => {
  const { children } = props;
  const [mounted, setMounted] = useState(false);
  const ref = useRef<HTMLDivElement>(null);

  return (
    <root.div>
      <DidMount onMount={() => setMounted(true)} /> <div ref={ref}></div>
      {mounted && ref.current && (
        <StyleSheetManager target={ref.current}>{children}</StyleSheetManager>
      )}
    </root.div>
  );
};

export default ShadowRoot;
