import React from "react";
import styled from "./TalkButton.styles";

interface Props {
  onClick?(): void;
}

const TalkButton: React.FC<Props> = (props) => {
  return (
    <styled.Button {...props}>
      <span></span>
      <span></span>
      <br />
      Hello
    </styled.Button>
  );
};

export default TalkButton;

TalkButton.defaultProps = {
  onClick: () => {}
};
