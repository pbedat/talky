import styled from "styled-components";

const Button = styled.button`
  text-align: left;
  padding: 1.1rem 2.25rem;
  border: 2px solid black;
  text-transform: uppercase;
  background: #9b4dca;
  font-weight: 600;
  color: #fff;
  font-size: 1.25rem;
  border: none;
  cursor: pointer;

  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);

  transition: color, background 0.3s;

  :hover {
    background: #333;
    /* color: #9b4dca; */
  }

  span:first-child {
    font-size: 1rem;

    ::before {
      content: "👋";
    }
  }

  span:nth-child(2) {
    ::before {
      content: "🤓";
    }
  }

  :hover span:nth-child(2) {
    ::before {
      content: "😎";
    }
  }

  :hover span:first-child {
    ::before {
      content: "👊";
    }
  }
`;

export default {
  Button
};
