import DependencyContainer from "../hooks/di";
import React from "react";
import App from "../components/App/App";
import configure from "../di";

interface Props {
  baseUrl: string;
}

const Talky: React.FC<Props> = (props) => {
  return (
    <DependencyContainer configure={() => configure(props.baseUrl)}>
      <App />
    </DependencyContainer>
  );
};

export default Talky;
