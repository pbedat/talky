import { Message } from "../types";
import { useEffect, useMemo, useReducer } from "react";
import uuid from "uuid";
import { useService } from "../di";
import { createAction, ActionType } from "typesafe-actions";

const actions = {
  load: createAction("LOAD_MESSAGES")<Message[]>(),
  push: createAction("PUSH_MESSAGE")<Message>(),
  changeStatus: createAction("CHANGE_STATUS")<boolean>()
};

interface State {
  isOnline: boolean;
  messages: Message[];
}

const reducer = (state: State, action: ActionType<typeof actions>): State => {
  switch (action.type) {
    case "LOAD_MESSAGES":
      return { ...state, messages: action.payload };
    case "PUSH_MESSAGE":
      return { ...state, messages: [...state.messages, action.payload] };
    case "CHANGE_STATUS":
      return { ...state, isOnline: action.payload };
  }
};

function runEffects(action: ActionType<typeof actions>) {
  try {
    switch (action.type) {
      case "PUSH_MESSAGE":
        if (
          Notification.permission === "granted" &&
          action.payload.isResponse
        ) {
          new Notification("pbedat.de", { body: "Patrick answered" });
        }
        break;
      case "CHANGE_STATUS":
        if (Notification.permission === "granted" && action.payload) {
          new Notification("pbedat.de", {
            body: "Patrick has come online"
          });
        }
        break;
    }
  } catch (err) {
    console.warn(`handling side effect of '${action.type}' failed`, err);
  }
}

export default function useTalky() {
  const ChannelEvents = useService("events");
  const Api = useService("api");
  const channelId = useMemo(getOrCreateChannelId, []);
  const [state, dispatch] = useReducer(reducer, {
    messages: [],
    isOnline: false
  });

  useEffect(() => {
    const events = ChannelEvents(channelId);

    events.addEventListener("message", ev => {
      try {
        const action: ActionType<typeof actions> = JSON.parse(ev.data);

        runEffects(action);

        dispatch(action);
      } catch (err) {
        console.warn("error parsing message", err);
      }
    });

    return () => events.close();
  }, []);

  async function submitMessage(from: string, text: string) {
    const [err] = await Api.postMessage({ channelId, text, from });

    if (err) {
      alert("Ooopsie, looks like an error occured... Please try again later");
    }
  }

  return {
    submitMessage,
    ...state
  };
}

function getOrCreateChannelId() {
  const CHANNEL_ID_KEY = "talk:channel-id";

  const channelId = window.localStorage.getItem(CHANNEL_ID_KEY) || uuid();
  window.localStorage.setItem(CHANNEL_ID_KEY, channelId);

  return channelId;
}
