/**
 * Micro dependency injection framework
 */

import React from "react";

export const DiContext = React.createContext<any>({});

interface Props {
  configure(): Record<string, () => unknown>;
}

const DependencyContainer: React.FC<Props> = (props) => {
  const { configure, children } = props;
  return (
    <DiContext.Provider value={configure()}>{children}</DiContext.Provider>
  );
};

export default DependencyContainer;
