import { useContext } from "react";
import { DiContext } from "./hooks/di";
import createInMemoryApi from "./services/in-memory-api";
import memoize from "lodash/memoize";
import mapValues from "lodash/mapValues";
import createBackendMock from "./services/backend-mock";
import createApiClient from "./services/http-api";

/**
 * Configure the dependency management by registering services here
 */
function configure(baseUrl: string) {
  const mockBackend = memoize(createBackendMock);

  const api = createInMemoryApi(mockBackend());

  function getStream(channelId: string) {
    // return mockBackend().getStream(channelId) as EventSource;

    return new EventSource(`${baseUrl}/${channelId}`);
  }

  return {
    events: () => getStream,
    //api: () => api
    api: () => createApiClient(baseUrl)
  };
}

export default configure;

export type Services = ReturnType<typeof configure>;

/**
 * Convenience method to retrieve registered dependencies
 * @param service
 */
export function useService<TServiceName extends keyof Services>(
  service: TServiceName,
  interceptors: Array<(serviceName: string, service: any) => any> = []
) {
  const ctxt = useContext(DiContext);

  const svc = ctxt[service]() as ReturnType<Services[TServiceName]>;

  return interceptors.reduce(
    (s, interceptor) => interceptor(service, s),
    svc
  ) as typeof svc;
}

export function logInterceptor(service: string, svc: any) {
  return mapValues(svc, (fn, key) => {
    if (typeof fn === "function") {
      return (...args: any[]) => {
        console.log(`[${service}] ${key}()`, args);
        const result = fn(...args);
        if (
          typeof result === "object" &&
          "then" in result &&
          typeof result.then === "function"
        ) {
          result.then((result: any) =>
            console.log(`[${service}] ${key}() =>`, result)
          );
        } else {
          console.log(`[${service}] ${key}() =>`, result);
        }
        return result;
      };
    }
  }) as typeof svc;
}
