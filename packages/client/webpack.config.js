const path = require("path");

module.exports = {
  externals: {
    react: "umd react",
    "react-dom": "umd react-dom"
  },
  entry: path.resolve(__dirname, "src/index.ts"),
  mode: "production",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "index.js",
    libraryTarget: "umd",
    library: "@talky/client"
  },
  resolve: { extensions: [".ts", ".tsx", ".js"] },
  module: {
    rules: [
      { test: /\.tsx?/, use: "ts-loader", exclude: /node_modules/ },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: "file-loader"
          },
          {
            loader: "image-webpack-loader"
          }
        ]
      }
    ]
  }
};
