import path from "path";
import notifier from "node-notifier";
import EventSource from "eventsource";
import yargs from "yargs";
import fetch from "node-fetch";
import cp from "child_process";

const { instanceId, url } = yargs
  .option("url", { required: true, type: "string" })
  .option("instanceId", { required: true, type: "string" }).argv;

async function main() {
  const response = await fetch(`${url}/online?access_token=${instanceId}`, {
    redirect: "manual"
  });

  if (response.status === 401) {
    process.stderr.write(`error: talky returned 401 -- invalid instance id\n`);
    process.exit(1);
  }

  if (response.status !== 302) {
    process.stderr.write(
      "error: expected redirect -- check that the `url` is a talky instance\n"
    );
    process.exit(1);
  }

  const location = response.headers.get("Location");

  if (location === null) {
    process.stderr.write("error: expected talky to return a location header");
    process.exit(1);
  }

  const events = new EventSource(location);

  events.onerror = (err: any) => {
    process.stderr.write("error: connection to the talky online beacon lost\n");
  };

  events.onopen = () => process.stdout.write("> you now appear online <\n");

  events.addEventListener("notify", notify);
}

main().catch(err => {
  process.stderr.write(`fatal error: ${err.message}\n`);
  console.debug(err.stack);
  process.exit(1);
});

notify();

function notify() {
  process.stdout.write(`${new Date().toISOString()} - NEW MESSAGE!\n`);

  notifier.notify({
    message: "New message!",
    title: "Talky"
  });

  cp.exec(`paplay ${path.resolve(__dirname, "../assets/bark.ogg")}`);
}
