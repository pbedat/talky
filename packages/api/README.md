# Talky Api

The talky API is used to establish communication `channels` for the talky "host" and
his guests.

The guests are sending their messages through the talky client to the channel, which relays the message
to the "host" via SMTP.

The host can respond to this channel, which is referenced in the subject of the email. By subscribing to a special email address, to which the API is listening with mail-fu, the responses are streamed back to the channel, where the guest can read them.
