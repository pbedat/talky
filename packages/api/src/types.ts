export interface ChannelMessage {
  isResponse?: boolean;
  from: string;
  text: string;
  time: Date;
}
