import { ChannelMessage } from "../types";
import fetch from "node-fetch";
import streambin from "../utils/streambin-client";

/**
 * This client is pushing messages to the streambin stream
 */
export default function createChannelClient() {
  /**
   * Publish a single message to the channel
   */
  function pushMessage(channelId: string, message: ChannelMessage) {
    return pushEvent(channelId, "PUSH_MESSAGE", message);
  }

  /**
   * Publish a list of messages to the channel
   */
  function loadMessages(channelId: string, messages: ChannelMessage[]) {
    return pushEvent(channelId, "LOAD_MESSAGES", messages || []);
  }

  /**
   * Notify the channel that the online status of the channel host has changed
   */
  function changeStatus(channelId: string, online: boolean) {
    return pushEvent(channelId, "CHANGE_STATUS", online);
  }

  /**
   * Push an event to streambin
   */
  function pushEvent(channelId: string, type: string, payload: any) {
    return streambin.push(`talky-${channelId}`, { type, payload });
  }

  return {
    pushMessage,
    loadMessages,
    changeStatus
  };
}

export type ChannelClient = ReturnType<typeof createChannelClient>;
