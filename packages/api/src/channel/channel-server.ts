import EventSource from "eventsource";
import { ChannelClient } from "./channel-client";
import messageStore from "../message-store";
import { OnlineBeacon } from "../online-beacon";

/**
 * Creates a server that is capable of hosting talky channels
 */
export default function createChannelServer(
  channelClient: ChannelClient,
  onlineBeacon: OnlineBeacon
) {
  let streams: Array<{ channelId: string; events: any }> = [];

  function isServing(channelId: string) {
    return streams.some(s => s.channelId === channelId);
  }

  function createStream(channelId: string) {
    return new Promise<string>((resolve, reject) => {
      const events = new EventSource(
        `https://streambin.pbedat.de/streams/talky-${channelId}`
      );

      events.onopen = () => {
        streams = [...streams, { channelId: channelId, events }];

        resolve(`https://streambin.pbedat.de/streams/talky-${channelId}/out`);
      };

      events.onerror = ev => {
        console.error("connecting to streambin failed", channelId, ev.data);
      };

      let connections = 0;

      events.addEventListener("connect", () => {
        connections++;

        channelClient.loadMessages(
          channelId,
          messageStore.getMessages(channelId)
        );

        channelClient.changeStatus(channelId, onlineBeacon.isOnline());
      });

      events.addEventListener("disconnect", () => {
        connections--;
        if (connections === 0) {
          streams = streams.filter(s => s.channelId !== channelId);
          close();
        }
      });

      const unsubscribe = onlineBeacon.subscribe(() =>
        streams.forEach(s =>
          channelClient.changeStatus(s.channelId, onlineBeacon.isOnline())
        )
      );

      function close() {
        events.close();
        unsubscribe();
      }

      setTimeout(() => {
        if (connections === 0) {
          streams = streams.filter(s => s.channelId !== channelId);
          close();
          reject("dropping stream due to timeout: no client connected");
        }
      }, 10000);
    });
  }

  return {
    isServing,
    createStream
  };
}

export type StreamServer = ReturnType<typeof createChannelServer>;
