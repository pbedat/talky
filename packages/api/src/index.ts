import express, { json } from "express";
import cors from "cors";
import { config } from "dotenv";
import createEmailApi from "./email-api";
import createChannelClient from "./channel/channel-client";
import createHttpApi from "./http-api";
import createChannelServer from "./channel/channel-server";
import createOnlineBeacon from "./online-beacon";
import bearerToken from "express-bearer-token";

config();

const instanceId = process.env.TALKY_INSTANCE_ID || "demo";
const emailAddress = process.env.TALKY_EMAIL_ADDRESS;
const displayName = process.env.TALKY_DISPLAY_NAME || "Demo";
const mailTransportUrl = process.env.MAIL_TRANSPORT_URL;

if (!emailAddress) {
  process.stdout.write("please provide a TALKY_EMAIL_ADDRESS");
  process.exit(1);
}

if (!mailTransportUrl) {
  process.stdout.write("please provide a MAIL_TRANSPORT_URL");
  process.exit(1);
}

process.stdout.write(`talky instance id: ${instanceId}\n`);

const app = express();

app.use(bearerToken());
app.use(cors());
app.use(json());

const streamClient = createChannelClient();
const onlineBeacon = createOnlineBeacon(instanceId);
const streamServer = createChannelServer(streamClient, onlineBeacon);
const api = createHttpApi(
  streamServer,
  streamClient,
  onlineBeacon,
  instanceId,
  emailAddress,
  mailTransportUrl
);

app.use(process.env.BASE_PATH || "/", api);

createEmailApi(streamClient, instanceId, emailAddress, displayName);

const port = process.env.PORT || 8080;

app.listen(port, () => {
  process.stdout.write(`listening to http://127.0.0.1:${port}\n`);
});
