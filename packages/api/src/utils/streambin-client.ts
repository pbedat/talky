import fetch from "node-fetch";

function push(stream: string, data: object, type: string = "message") {
  return fetch(`https://streambin.pbedat.de/streams/${stream}/in`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      type,
      data: JSON.stringify(data)
    })
  });
}

const streambin = { push };

export default streambin;
