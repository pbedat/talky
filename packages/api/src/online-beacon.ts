import EventSource from "eventsource";
import streambin from "./utils/streambin-client";

/**
 * Creates a specific stream, that controls your talky online status:
 * Whenever you connect to the stream, you will appear online.
 */
export default function createOnlineBeacon(instanceId: string) {
  let online = 0;
  let listeners: Array<() => void> = [];

  const onlineBeacon = new EventSource(
    `https://streambin.pbedat.de/streams/talky-${instanceId}-online`
  );

  onlineBeacon.onopen = () => console.log("onlineBeacon ok");

  onlineBeacon.onerror = () => {
    console.error("ONLINE ERR");
  };

  onlineBeacon.addEventListener("connect", () => {
    console.log("ONLINE");
    online++;

    if (online !== 1) {
      return;
    }

    listeners.forEach(notify => notify());
  });

  onlineBeacon.addEventListener("disconnect", () => {
    online--;
    console.log("OFFLINE");

    if (online === 0) {
      listeners.forEach(notify => notify());
    }
  });

  return {
    isOnline() {
      return online > 0;
    },
    subscribe(notify: () => void) {
      listeners.push(notify);

      return () => (listeners = listeners.filter(n => n === notify));
    },
    notify() {
      return streambin.push(
        `talky-${instanceId}-online`,
        { timestamp: new Date().toISOString() },
        "notify"
      );
    }
  };
}

export type OnlineBeacon = ReturnType<typeof createOnlineBeacon>;
