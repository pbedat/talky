import { ChannelMessage } from "./types";

let channels: Record<string, ChannelMessage[]> = {};

function postMessage(channelId: string, message: ChannelMessage) {
  channels = {
    ...channels,
    [channelId]: channels[channelId]
      ? [...channels[channelId], message]
      : [message]
  };
}

function getMessages(channelId: string) {
  return channels[channelId];
}

const messageStore = {
  postMessage,
  getMessages
};

/**
 * Persists channel messages through talky sessions
 */
export default messageStore;
