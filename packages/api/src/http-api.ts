import { Router } from "express";
import messageStore from "./message-store";
import { createTransport } from "nodemailer";
import { ChannelClient } from "./channel/channel-client";
import { StreamServer } from "./channel/channel-server";
import { OnlineBeacon } from "./online-beacon";

function createHttpApi(
  streamServer: StreamServer,
  streamClient: ChannelClient,
  onlineBeacon: OnlineBeacon,
  instanceId: string,
  emailAddress: string,
  mailTransportUri: string
) {
  const router = Router();

  router.get("/", (req, res) => {
    res.send("talky").end();
  });

  router.get("/online", (req, res) => {
    if (req.token !== instanceId) {
      return res.status(401).end();
    }

    res.redirect(
      `https://streambin.pbedat.de/streams/talky-${instanceId}-online/out`
    );
    res.end();
  });

  const smtp = createTransport(mailTransportUri);

  /**
   * Relay messages to the talky mailbox
   */
  router.post("/:channelId/messages", async (req, res) => {
    const { channelId } = req.params;
    const message = { time: new Date(), ...req.body };

    messageStore.postMessage(channelId, message);

    try {
      await smtp.sendMail({
        to: emailAddress,
        replyTo: {
          name: message.from,
          address: `talky-${instanceId}@pbedat.de`
        },
        subject: channelId,
        text: [message.time, message.text].join("\n\n")
      });
    } catch (err) {
      console.error("could not send mail", err);
    }

    try {
      await onlineBeacon.notify();
    } catch (err) {
      console.error("beacon notify", err);
    }

    const response = await streamClient.pushMessage(channelId, message);

    res
      .status(response.ok ? 201 : 400)
      .json(response.ok ? { message: "OK" } : { error: "no stream" });
  });

  /**
   * creates a new event stream if non exists and forwards to the event stream url
   */
  router.get("/:channelId", async (req, res) => {
    if (streamServer.isServing(req.params.channelId)) {
      res.redirect(
        `https://streambin.pbedat.de/streams/talky-${req.params.channelId}/out`
      );
      res.end();
      return;
    }

    // TODO handle error
    res.redirect(await streamServer.createStream(req.params.channelId));
    res.end();
  });
  return router;
}

export default createHttpApi;
