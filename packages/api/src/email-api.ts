import messageStore from "./message-store";
import EventSource from "eventsource";
import fetch from "node-fetch";
import { ChannelClient } from "./channel/channel-client";

// TODO generalize

/**
 * Creates an mail-fu stream to receive responses to a talky channel via email
 */
export default function createEmailApi(
  streamClient: ChannelClient,
  instanceId: string,
  emailAddress: string,
  displayName: string
) {
  const mails = new EventSource(
    `https://mail-fu.pbedat.de/api/pbedat.de/talky-${instanceId}/events`
  );

  mails.addEventListener("message", relayToChannelStream);

  function relayToChannelStream(ev: any) {
    const { text, subject, from } = JSON.parse(ev.data);

    console.log("mail", { text, subject, from }, from.value.address);

    if (from.value[0].address !== emailAddress) {
      return;
    }

    const channelId = subject.replace(/RE:\s*/i, "").trim();

    const message = {
      isResponse: true,
      from: displayName,
      time: new Date(),
      text
    };

    messageStore.postMessage(channelId, message);
    streamClient.pushMessage(channelId, message);
  }
}

export type EmailResponseApi = ReturnType<typeof createEmailApi>;
